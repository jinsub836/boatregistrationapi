package com.js.boatregistrationapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoatRegistrationApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoatRegistrationApiApplication.class, args);
    }

}
