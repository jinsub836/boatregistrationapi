package com.js.boatregistrationapi.controller;

import com.js.boatregistrationapi.model.FishBoatRequest;
import com.js.boatregistrationapi.service.FishBoatService;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/boat")
public class FishBoatController {
    private final FishBoatService fishBoatService;

    @PostMapping("/new")
    public String setFishBoat(@RequestBody FishBoatRequest request) {
        fishBoatService.setFishBoat(request);
        return "등록이 완료 되었습니다.";
    }
}