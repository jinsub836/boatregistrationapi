package com.js.boatregistrationapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FishBoatRequest {
    private String ownerName;
    private String boatName;
    private String target;
    private Integer boatNumber;
}
